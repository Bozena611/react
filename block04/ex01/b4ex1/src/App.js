import React from 'react';
import './App.css';

class Counter extends React.Component {

  state = {
    clicks: 1, /*{when 0 counts even only, not sure putting 1 is correct}*/
    counter: 0,
  }

handleClick = () => {
    let { counter, clicks } = this.state;
    this.setState({ clicks: clicks + 1 });
    if (this.state.clicks % 2 !== 0) {
      this.setState({ counter: counter + 1 });
      console.log (this.state);
    }
  };


  render() {
    return (
      <div className = 'display'>
        <div className='counter'>{this.state.counter}</div>
          <button onClick={this.handleClick}>+</button>
      </div>
    );
  }
}


export default Counter;
