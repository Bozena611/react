import React from 'react'

class Footer extends React.Component {

	render() {
		return(
			<div>
				<footer>
					<h3>Get in touch</h3>
					<p>Give us a call:</p>
					<p><u>+34 938 325 516</u></p>
					<p className='copy'>Copyright &copy; 2019</p>
				</footer>
			</div>
			)
	}
}


export default Footer
