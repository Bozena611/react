import React from 'react';
import './index.css';
import Header from './Header'; 
import App from './App'; 
import Footer from './Footer';


class Main extends React.Component{
  render() {

  	return (
      <div>
        <Header className='nav'>
        </Header>
        <div>
          <App />
        </div>
        <Footer/>
      </div>
    );
  }
}

export default Main;