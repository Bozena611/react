import React from 'react';
import SingleProduct from './SingleProduct';

function ProductsList (props) {
	return (
			<div className='mainContainer'>
			{
				props.product.map(function(ele, i) {
				return (
					<div key={i}>
						<SingleProduct product={ele}/>
					</div>
					)

				})
			}
			</div>
			)
	
}

export default ProductsList;
