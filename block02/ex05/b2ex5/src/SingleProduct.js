import React from 'react';
import './index.css';
function SingleProduct (props) {
	
		return (
			
			
			<div className='single'>
				<h2 className='color'>{props.product.product}</h2>
				 <img title = {props.product.product} src= {props.product.image} alt='product'/>
				 <div>
				 	<h4>Price: €{props.product.price}</h4>
				 </div>
			</div>
			
			
			)
}

export default SingleProduct;